(function($) {

  // Init Wow
  wow = new WOW({
    animateClass: 'animated',
    offset: 100
  });
  wow.init();

  $(".navbar-collapse a").on('click', function() {
    $(".navbar-collapse.collapse").removeClass('in');
  });

  // Navigation scrolls
  $('.navbar-nav li a').bind('click', function(event) {
    $('.navbar-nav li').removeClass('active');
    $(this).closest('li').addClass('active');
    var $anchor = $(this);
    var nav = $($anchor.attr('href'));
    if (nav.length) {
      $('html, body').stop().animate({
        scrollTop: $($anchor.attr('href')).offset().top
      }, 1500, 'easeInOutExpo');

      event.preventDefault();
    }
  });

  // About section scroll
  $(".overlay-detail a").on('click', function(event) {
    event.preventDefault();
    var hash = this.hash;
    $('html, body').animate({
      scrollTop: $(hash).offset().top
    }, 900, function() {
      window.location.hash = hash;
    });
  });

  //jQuery to collapse the navbar on scroll
  $(window).scroll(function() {
    if ($(".navbar-default").offset().top > 50) {
      $(".navbar-fixed-top").addClass("top-nav-collapse");
    } else {
      $(".navbar-fixed-top").removeClass("top-nav-collapse");
    }
  });

  // Testimonials Slider
  $('.bxslider').bxSlider({
    adaptiveHeight: true,
    mode: 'fade'
  });

// Nokhetha Slider image Fadein, Fadeout 

$next = 1;      // fixed, please do not modfy;
  $current = 0;   // fixed, please do not modfy;
  $interval = 4000; // You can set single picture show time;
  $fadeTime = 800;  // You can set fadeing-transition time;
  $imgNum = 7;    // How many pictures do you have
 
  $(document).ready(function(){
    //NOTE : Div Wrapper should with css: relative;
    //NOTE : img should with css: absolute;
    //NOTE : img Width & Height can change by you;
    $('.fadeImg').css('position','relative');
    $('.fadeImg img').css({'position':'absolute','width':'292px','height':'462px','left':'2px','top':'0px'});
 
    nextFadeIn();
  });
 
  function nextFadeIn(){
    //make image fade in and fade out at one time, without splash vsual;
    $('.fadeImg img').eq($current).delay($interval).fadeOut($fadeTime)
    .end().eq($next).delay($interval).hide().fadeIn($fadeTime, nextFadeIn);
        
    // if You have 5 images, then (eq) range is 0~4 
    // so we should reset to 0 when value > 4; 
    if($next < $imgNum-1){ $next++; } else { $next = 0;}
    if($current < $imgNum-1){ $current++; } else { $current =0; }
  };

// Left Main Menu Script 

var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
        body = document.body;

      showLeft.onclick = function() {
        classie.toggle( this, 'active' );
        classie.toggle( menuLeft, 'cbp-spmenu-open' );
        disableOther( 'showLeft' );
      };    

      function disableOther( button ) {
        if( button !== 'showLeft' ) {
          classie.toggle( showLeft, 'disabled' );
        }
        
      }




})(jQuery);
